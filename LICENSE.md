# Creative Commons Attribution-NoDerivatives 4.0 International Public License

1. This license only covers the **pali text in tipitaka.lk**
2. Modifications are not allowed - if you see any errors they should be reported to us rather than correcting on your end. If we deem that corrections are needed we will perform them on our end first and then you can get the updated files back from this github repo.
3. Attribution should be given to tipitaka.lk

For more info https://creativecommons.org/licenses/by-nd/4.0/legalcode
